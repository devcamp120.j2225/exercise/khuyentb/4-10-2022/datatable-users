import DataTable from "./Components/DataTable";
import "bootstrap/dist/css/bootstrap.min.css"

function App() {
  return (
    <div>
      <DataTable></DataTable>
    </div>
  );
}

export default App;
