import { Container } from "@mui/system";
import { Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Typography, Button, Pagination, Select, FormControl, InputLabel, MenuItem, PaginationItem } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PAGINATION_CHANGE } from "../Constant/PaginationTypeAction"
import ModalUser from "./ModalUser";
import { NUMBER_PAGE_CHANGE } from "../Constant/NumberPageTypeAction";


function DataTable() {

    const dispatch = useDispatch();

    const { currentPage } = useSelector((reduxData) => reduxData.PaginationReducer);
    const { page } = useSelector((reduxData) => reduxData.NumberPageReducer);

    const [rows, setRows] = useState([])
    const [totalPage, setTotalPage] = useState(0)
    const [openModal, setOpenModal] = useState(false)
    const [userInfo, setUserInfo] = useState([])


    const fetchAPI = async (url, responseOption) => {
        const response = await fetch(url, responseOption);
        const data = await response.json();
        return data;
    }

    useEffect(() => {
        var responseOption = {
            method: "GET",
            redirect: "follow"
        }

        fetchAPI("http://203.171.20.210:8080/devcamp-register-java-api/users", responseOption)
            .then((data) => {
                setTotalPage(Math.ceil(data.length / page))
                setRows(data.slice((currentPage - 1) * page, currentPage * page))
            })
            .catch((err) => {
                console.log(err.message)
            })
    }, [currentPage, page])

    const onChangePageHandler = (event, value) => {
        dispatch({
            type: PAGINATION_CHANGE,
            payload: value
        })
    }

    const onClickOpenModalHandler = (row) => {
        setOpenModal(true)
        setUserInfo(row)
        console.log(row)
    }

    const onClickCloseModalHandler = () => {
        setOpenModal(false)
    }

    const OnPageChangeHandler = (event) => {
        
        dispatch({
            type: NUMBER_PAGE_CHANGE,
            payload: event.target.value
        })
    }
    return (
        <Container>
            <Grid className="text-center mt-5 mb-5" >
                <Typography style={{ fontWeight: "bold", fontSize: "40px" }}>DANH SÁCH ĐĂNG KÝ</Typography>
            </Grid>
            <Grid container>
                <Grid item lg={2}>
                    <InputLabel id="demo-simple-select-label">Select Page Number</InputLabel>
                </Grid>
                <Grid item lg={10}>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        defaultValue={page}
                        label="Page"
                        onChange={OnPageChangeHandler}
                    >
                        <MenuItem value={5}>Five</MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={15}>Fifteen</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                    </Select>
                </Grid>
            </Grid>
            <Grid container mt={5}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell align="center">Firstname</TableCell>
                                <TableCell align="center">Lastname</TableCell>
                                <TableCell align="center">Country</TableCell>
                                <TableCell align="center">Subject</TableCell>
                                <TableCell align="center">Customer Type</TableCell>
                                <TableCell align="center">Register Status</TableCell>
                                <TableCell align="center">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow
                                    key={row.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">{row.id}</TableCell>
                                    <TableCell align="center">{row.firstname}</TableCell>
                                    <TableCell align="center">{row.lastname}</TableCell>
                                    <TableCell align="center">{row.country}</TableCell>
                                    <TableCell align="center">{row.subject}</TableCell>
                                    <TableCell align="center">{row.customerType}</TableCell>
                                    <TableCell align="center">{row.registerStatus}</TableCell>
                                    <TableCell><Button color="info" variant="contained" onClick={() => onClickOpenModalHandler(row)}>Chi Tiết</Button></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid container justifyContent={"end"}>
                <Grid item>
                    <Pagination count={totalPage} defaultPage={currentPage} onChange={onChangePageHandler}></Pagination>
                </Grid>
            </Grid>
            <ModalUser
                openProps={openModal}
                closeProps={onClickCloseModalHandler}
                userInfoProps={userInfo}
            ></ModalUser>
        </Container>

    )
}

export default DataTable;