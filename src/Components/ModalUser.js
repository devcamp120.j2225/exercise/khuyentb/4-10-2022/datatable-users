import { Modal, Box, Button, Typography} from "@mui/material";

function ModalUser({openProps, closeProps, userInfoProps}) {
    return (
        <>
            <Modal
                open={openProps}
                onClose={closeProps}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box
                    style={{
                        margin: "100px",
                        marginLeft: "500px",
                        backgroundColor: "white",
                        paddingLeft: "200px",
                        paddingTop: "100px",
                        paddingBottom: "100px",
                        border: "solid 2px",
                        width: "600px"
                    }}
                >
                    
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Thông tin chi tiết user
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 4 }}>
                        User ID: {userInfoProps.id}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                        First Name: {userInfoProps.firstname}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                        Last Name: {userInfoProps.lastname}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                        Country: {userInfoProps.country}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                        Subject: {userInfoProps.subject}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                        Customer Type: {userInfoProps.customerType}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                        Register Status: {userInfoProps.registerStatus}
                    </Typography>
                    <Button onClick={closeProps} color="info" variant="contained" style={{marginLeft: "50px", marginTop: "20px"}}>Close</Button>
                </Box>
            </Modal>
        </>

    )
}

export default ModalUser;