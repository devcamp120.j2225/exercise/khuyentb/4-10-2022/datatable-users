import { PAGINATION_CHANGE } from "../Constant/PaginationTypeAction"

const intialState = {
    currentPage: 1
}

function PaginationReducer (state = intialState, action) {
    switch(action.type) {
        case PAGINATION_CHANGE:
            return {
                ...state,
                currentPage: action.payload
            }
        default:
            return state;
    }
}

export default PaginationReducer;