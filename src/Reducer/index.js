import { combineReducers } from "redux";
import PaginationReducer from "./PaginationReducer"
import NumberPageReducer from "./NumberPageReducer";


const rootReducer = combineReducers({
    PaginationReducer,
    NumberPageReducer
});

export default rootReducer