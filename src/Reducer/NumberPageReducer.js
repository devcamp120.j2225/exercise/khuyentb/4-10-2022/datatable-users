import {NUMBER_PAGE_CHANGE} from "../Constant/NumberPageTypeAction"

const intialState = {
    page: 10
}

function NumberPageReducer (state = intialState, action) {
    switch(action.type) {
        case NUMBER_PAGE_CHANGE: 
            return {
                ...state,
                page: action.payload
            }
        default: {
            return state
        }
    }
}

export default NumberPageReducer